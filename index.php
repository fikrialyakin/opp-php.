 <?php

 require('Animals.php');
 require('Frog.php');
 require('Ape.php');

 $sheep = new Animals("Shaun");

 echo "nama hewan : $sheep->name <br>";
 echo "jumlah kaki : $sheep->legs<br>";
 echo "cold blooded : $sheep->cold_blooded <br>" ;
 echo "<br>";

 $sungokong = new Ape("kera sakti");

 echo "nama hewan : $sungokong->name<br>";
 echo "Jumlah kaki  : $sungokong->legs<br>";
 echo "cold blooded : $sungokong->cold_blooded<br>";
 echo "yell : $sungokong->yell<br>";
 echo "<br>";

 $kodok =new Frog("buduk");

 echo "nama hewan : $kodok->name<br>";
 echo "Jumlah kaki  : $kodok->legs<br>";
 echo "cold blooded : $kodok->cold_blooded<br>";
 echo "jump : $kodok->jump<br>"; 

 ?>

